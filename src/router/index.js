import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from '@/components/StartScreen.vue'
import QuestionScreen from '@/components/QuestionScreen.vue'
import ResultScreen from '@/components/ResultScreen.vue'


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'StartScreen',
        component: StartScreen,
    },
    {
        path: '/questions',
        name: 'QuestionScreen',
        component: QuestionScreen,
    },
    {
        path: '/results',
        name: 'ResultScreen',
        component: ResultScreen,
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
})

export default router