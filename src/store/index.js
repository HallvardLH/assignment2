import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // Keep arrays for all of the questions, answers and types fetched
        questions: [],
        correctAnswers: [],
        wrongAnswers: [],
        questionTypes: [], 
        difficultyChosen: '',
        numberOfQuestionsChosen: 0,
        categoryChosen: '',
        count: 0,
        // Keep array user responses
        userAnswers: [],
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setCorrectAnswers: (state, payload) => {
            state.correctAnswers = payload
        },
        setWrongAnswers: (state, payload) => {
            state.wrongAnswers = payload
        },
        setQuestionTypes: (state, payload) => {
            state.questionTypes = payload
        },
        setDifficulty: (state, payload) => {
            state.difficultyChosen = payload
        },
        setNumberOfQuestionsChosen: (state, payload) => {
            state.numberOfQuestionsChosen = payload
        },
        setCategoryChosen: (state, payload) => {
            state.categoryChosen = payload
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers = payload
        },
        setCount: (state, payload) => {
            state.count = payload
        },
    },
    //getters: {},
    actions: {
        // Start the quiz and change to QuestionScreen component
        async startQuiz({ commit }, args){

            const baseUrl = 'https://opentdb.com/api.php?'
            // Difficulty, amount of questions and category have been chosen
            // by user. Use them in the query.
            const url = baseUrl + args
            console.log("url:", url);

            const response = await fetch(url)
            const json = await response.json()
            const results = json["results"]

            let correctAnswers = []
            let wrongAnswers = []
            let questionTypes = []
            let questions = []
            // Loop through results and populate arrays
            for (let i = 0; i < results.length; i++) {
                correctAnswers.push(results[i].correct_answer)
                wrongAnswers.push(results[i].incorrect_answers)
                questionTypes.push(results[i].type)
                questions.push(results[i].question)
            }
            commit('setQuestions', questions)
            commit('setWrongAnswers', wrongAnswers)        
            commit('setQuestionTypes', questionTypes)        
            commit('setCorrectAnswers', correctAnswers)
        },
        // Function clears all data
        resetAllData({ commit }){
            commit('setQuestions', [])
            commit('setWrongAnswers', [])        
            commit('setQuestionTypes', [])        
            commit('setCorrectAnswers', [])
            commit('setDifficulty', '')        
            commit('setNumberOfQuestionsChosen', 0)        
            commit('setCategoryChosen', '')
            commit('setCount', 0)
            commit('setUserAnswers', [])
        },
        // Function clears all data except difficulty, number of questions and category
        resetDataForReplay({ commit }){
            commit('setQuestions', [])
            commit('setWrongAnswers', [])        
            commit('setQuestionTypes', [])        
            commit('setCorrectAnswers', [])
            commit('setCount', 0)
            commit('setUserAnswers', [])
        }
    }
})